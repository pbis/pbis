<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crimes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('crime_nature_id');
            $table->string('crime_street_address');
            $table->string('crime_city');
            $table->string('crime_state');
            $table->string('crime_zip');
            $table->string('investigation_officer');
            $table->date('date_of_crime');
            $table->integer('victim_id')->nullable();
            $table->string('victim_surname');
            $table->string('victim_fname');
            $table->string('victim_mname');
            $table->string('victim_lname');
            $table->string('victim_street_Address');
            $table->string('victim_city');
            $table->string('victim_state');
            $table->string('victim_zip');
            $table->string('victim_nationality');
            $table->date('victim_date_of_birth');
            $table->integer('victim_age');
            $table->string('victim_sex');
            $table->integer('case_number');
            $table->string('witness_fname');
            $table->string('witness_lname');
            $table->string('witness_mname');
            $table->string('witness_street_address');
            $table->string('witness_city');
            $table->string('witness_state');
            $table->string('witness_zip');
            $table->string('witness_nationality');
            $table->date('witness_date_of_birth');
            $table->integer('witness_age');
            $table->integer('criminal_id')->nullable();
            $table->string('criminal_crime_description');
            $table->string('criminal_fname');
            $table->string('criminal_mname');
            $table->string('criminal_lname')->nullable();
            $table->string('criminal_street_address');
            $table->string('criminal_city');
            $table->string('criminal_state');
            $table->string('criminal_zip');
            $table->string('criminal_nationality');
            $table->date('criminal_date_of_birth');
            $table->date('criminal_Date_of_arrest');
            $table->integer('criminal_age');
            $table->string('nature_of_crime');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crimes');
    }
}
