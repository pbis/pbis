<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PrisonerMovement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('prisoner_movements', function (Blueprint $table){
            $table->increments('id');
            $table->integer('prisoner_id');
            $table->integer('driver_id');
            $table->integer('guard1_id');
            $table->integer('guard2_id');
            $table->integer('guard3_id');
            $table->string('from');
            $table->string('to');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExist('PrisonerMovement');
    }
}
