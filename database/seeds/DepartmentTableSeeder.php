<?php

use Illuminate\Database\Seeder;
use App\Department;
use Faker\Factory as Faker;
use Carbon\Carbon;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $facker = Faker::create();
        Department::create([
            'name' => "Hr",
            'description' => 'Hr',
             'created_at' => Carbon::now(),
             'updated_at' => Carbon::now()
         ]);
    }
}
