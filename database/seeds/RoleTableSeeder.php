<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;
use Carbon\Carbon;
use Faker\Factory as Faker;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = ["view_departments", "view_permissions", "view_prisoners", "view_prisoners_movement", "view_roles", "view_users"];
        $id = [];
        for ($i = 0; $i < count($permissions); $i++) {
            $permission = Permission::create([
                'name' => $permissions[$i],
                 'created_at' => Carbon::now(),
                 'updated_at' => Carbon::now()
             ]);
             array_push($id, $permission['id']);
        }

        Role::create([
            'name' => 'Admin',
             'created_at' => Carbon::now(),
             'updated_at' => Carbon::now()
         ])->permissions()->attach($id);
    }
}
