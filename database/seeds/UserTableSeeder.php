<?php

use Illuminate\Database\Seeder;
use App\User;
use Carbon\Carbon;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
           'surname' => 'Mr.',
           'first_name' => 'abebe',
            'middle_name' => 'bekele',
            'last_name' => 'kebede',
            'sex' => 'male',
            'nationality' => 'ethiopian',
            'birth_place' => 'addis ababa',
            'birth_date' => '12-11-19',
            'department_id' => 1,
            'role_id' => 1,
            'email' => 'abebe@gmail.com',
            'password' => bcrypt('password'),
            'active' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
