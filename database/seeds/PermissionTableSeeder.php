<?php

use Illuminate\Database\Seeder;
use App\Permission;
use Carbon\Carbon;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create([
            'name' => 'All',
             'created_at' => Carbon::now(),
             'updated_at' => Carbon::now()
         ]);
    }
}
