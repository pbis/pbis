<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/users', 'UserController@index');
Route::get('/user/{id}', 'UserController@show');
Route::get('/users/department/{id}', 'UserController@getByDepartment');
Route::patch('/user/{id}', 'UserController@update');
Route::post('/user', 'UserController@store');
Route::get('/user/activate/{id}', 'UserController@activate');
Route::get('/user/deactivate/{id}', 'UserController@deactivate');
Route::get('/users/count', 'UserController@count');

// department
Route::get('/departments', 'DepartmentController@index');
Route::delete('/department/{id}', 'DepartmentController@destroy');
Route::patch('/department/{id}', 'DepartmentController@update');
Route::post('/department', 'DepartmentController@store');
Route::get('/departments/count', 'DepartmentController@count');

// permission
Route::get('/permissions', 'PermissionController@index');
Route::delete('/permission/{id}', 'PermissionController@destroy');
Route::patch('/permission/{id}', 'PermissionController@update');
Route::post('/permission', 'PermissionController@store');

// role
Route::get('/roles', 'RoleController@index');
Route::get('/roles/withPermission', 'RoleController@roleWithPermission');
Route::delete('/role/{id}', 'RoleController@destroy');
Route::patch('/role/{id}', 'RoleController@update');
Route::post('/role', 'RoleController@store');

// prisoners
Route::get('/prisoners', 'PrisonerController@index');
Route::delete('/prisoners/{id}', 'PrisonerController@destroy');
Route::patch('/prisoners/{id}', 'PrisonerController@update');
Route::post('/prisoners', 'PrisonerController@store');
Route::get('/prisoners/count', 'PrisonerController@count');

// payroll
Route::get('/payroll', 'PayrollController@index');
Route::delete('/payroll/{id}', 'PayrollController@destroy');
Route::patch('/payroll/{id}', 'PayrollController@update');
Route::post('/payroll', 'PayrollController@store');

// biometric
Route::get('/biometric', 'BiometricController@index');
Route::delete('/biometric/{id}', 'BiometricController@destroy');
Route::get('/biometric/prisoner/{id}', 'BiometricController@getBiometrics');
Route::patch('/biometric/{id}', 'BiometricController@update');
Route::post('/biometric', 'BiometricController@store');

// firearm
Route::get('/firearms', 'FirearmController@index');
Route::get('/firearm/{id}', 'FirearmController@show');
Route::delete('/firearm/{id}', 'FirearmController@destroy');
Route::patch('/firearm/{id}', 'FirearmController@update');
Route::post('/firearm', 'FirearmController@store');
Route::get('/firearm/verify/{id}', 'FirearmController@verify');
Route::get('/firearm/unverify/{id}', 'FirearmController@unverify');
Route::get('/firearms/count', 'FirearmController@count');

//Firearm type
Route::get('/firearm-types', 'FirearmTypeController@index');
Route::delete('/firearm-type/{id}', 'FirearmTypeController@destroy');
Route::patch('/firearm-type/{id}', 'FirearmTypeController@update');
Route::post('/firearm-type', 'FirearmTypeController@store');

// attendance
Route::get('/attendance/{id}/{year}/{month}', 'AttendanceController@show');
Route::patch('/attendance/{id}', 'AttendanceController@update');
Route::post('/attendance', 'AttendanceController@store');

// demographic data
Route::get('/demographic-datas', 'DemographicDataController@index');
Route::delete('/demographic-data/{id}', 'DemographicDataController@destroy');
Route::get('/demographic-data/prisoner/{id}', 'DemographicDataController@getPrisonerDemographics');
Route::patch('/demographic-data/{id}', 'DemographicDataController@update');
Route::post('/demographic-data', 'DemographicDataController@store');

// notification
Route::get('/notifications/{id}', 'NotificationController@index');
Route::delete('/notification/{id}', 'NotificationController@destroy');
Route::patch('/notification/{id}', 'NotificationController@update');
Route::post('/notification', 'NotificationController@store');

// forensic module
Route::get('/forensic-modules', 'ForensicModuleController@index');
Route::get('/forensic-module/{id}', 'ForensicModuleController@show');
Route::delete('/forensic-module/{id}', 'ForensicModuleController@destroy');
Route::patch('/forensic-module/{id}', 'ForensicModuleController@update');
Route::post('/forensic-module', 'ForensicModuleController@store');

// visitor
Route::get('/visitors', 'VisitorController@index');
Route::get('/visitor/{id}', 'VisitorController@show');
Route::delete('/visitor/{id}', 'VisitorController@destroy');
Route::patch('/visitor/{id}', 'VisitorController@update');
Route::post('/visitor', 'VisitorController@store');

// Training
Route::get('/training', 'trainingController@index');
Route::delete('/training/{id}', 'trainingController@destroy');
Route::patch('/training/{id}', 'trainingController@update');
Route::post('/training', 'trainingController@store');

// Prisoner movement
Route::get('/prisonerMovement', 'PrisonerMovementController@index');
Route::delete('/prisonerMovement/{id}', 'PrisonerMovementController@destroy');
Route::patch('/prisonerMovement/{id}', 'PrisonerMovementController@update');
Route::post('/prisonerMovement', 'PrisonerMovementController@store');

// work
Route::get('/work', 'WorkController@index');
Route::delete('/work/{id}', 'WorkController@destroy');
Route::patch('/work/{id}', 'WorkController@update');
Route::post('/work', 'WorkController@store');

//medical appointment
Route::get('/medicalAppointment', 'MedicalAppointmentController@index');
Route::delete('/medicalAppointment/{id}', 'MedicalAppointmentController@destroy');
Route::patch('/medicalAppointment/{id}', 'MedicalAppointmentController@update');
Route::post('/medicalAppointment', 'MedicalAppointmentController@store');

//medical status
Route::get('/medicalStatus', 'MedicalStatusController@index');
Route::delete('/medicalStatus/{id}', 'MedicalStatusController@destroy');
Route::patch('/medicalStatus/{id}', 'MedicalStatusController@update');
Route::post('/medicalStatus', 'MedicalStatusController@store');

//healthcare professional
Route::get('/healthcareProfessional', 'healthcareProfessionalController@index');
Route::delete('/healthcareProfessional/{id}', 'healthcareProfessionalController@destroy');
Route::patch('/healthcareProfessional/{id}', 'healthcareProfessionalController@update');
Route::post('/healthcareProfessional', 'healthcareProfessionalController@store');

//crime
Route::get('/crime', 'crimeController@index');
Route::delete('/crime/{id}', 'crimeController@destroy');
Route::patch('/crime/{id}', 'crimeController@update');
Route::post('/crime', 'crimeController@store');