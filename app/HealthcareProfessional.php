<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HealthcareProfessional extends Model
{
   protected $fillable =[
       'first_name','middle_name','phone_number','education','last_name','age','sex','current_activity','email'
      
   ];
}
