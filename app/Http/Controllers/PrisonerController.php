<?php

namespace App\Http\Controllers;

use App\Prisoner;
use Illuminate\Http\Request;

class PrisonerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     return Prisoner::latest()->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response`
     */
    public function store(Request $request)
    {
        //return['message'=>'I have your data'];
        $this->validate($request,[
         'name'  => 'required|string|max:191',
         
         'birthplace' => 'required|string|max:191',
         'nationality'=>'required|string|max:191',
         'age'=>'required|numeric|max:191',

        ]);
       return Prisoner::create([
'name'=> $request['name'],

'gender'=> $request['gender'],
'age'=> $request['age'],
'birthdate'=> $request['birthdate'],
'birthplace'=> $request['birthplace'],
'nationality'=> $request['nationality'],
'address'=> $request['address'],
'bio'=> $request['bio'],




       ]);
    }

    public function count()
    {
        return response(Prisoner::all()->count());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         // return['message'=>'I have your data'];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $prisoner = Prisoner::findOrFail($id);
         $this->validate($request,[
         'name'  => 'required|string|max:191',
    
         //'password'=> 'sometimes|min:8',
         'birthplace' => 'required',
          'nationality'=>'required'

        ]);


        $prisoner ->update($request->all());
        return['message'=>'Updated the prisoner info'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $prisoner = Prisoner::findOrFail($id);
 
        $prisoner -> delete();
        //delete the Prisoner
        return['message'=>'Prisoner Deleted'];
    }
}
