<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DemographicData;
use Illuminate\Support\Facades\Validator;

class DemographicDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(DemographicData::all());
    }

    public function getPrisonerDemographics($id)
    {
        return response()->json(DemographicData::where('prisoner_id', $id)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'prisoner_id' => 'required',
            'photo' => 'required',
            'item_name' => 'required',
            'count' => 'required'
        ]);     

        if ($validator->fails()) {
            return response(['demographicData' => 'error', 'error' => $validator->errors()]);
        }

        $photo_name = time().'.'.$request->photo->getClientOriginalExtension();
        $request->photo->move(public_path('images/demographics'), $photo_name);

        DemographicData::create([
           'prisoner_id' => $request->prisoner_id,
            'photo' => $photo_name,
            'item_name' => $request->item_name,
            'count' => $request->count
        ]);

        return response(['demographicData' => 'created']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(DemographicData::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'prisoner_id' => 'required',
            'photo' => 'required',
            'item_name' => 'required',
            'count' => 'required'
        ]);

        if ($validator->fails()) {
            return response(['demographicData' => 'error', 'error' => $validator->errors()]);
        }

        // TODO :: upload the photo and store the path in $photo_name

        $photo_name = 'photo';

        DemographicData::find($id)->update([
                'prisoner_id' => $request->prisoner_id,
                'photo' => $request->photo,
                'item_name' => $request->item_name,
                'count' => $request->count
            ]);

        return response(['demographicData' => 'updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DemographicData::find($id)->delete();
        return response()->json(["demographicData" => "deleted"]);
    }
}
