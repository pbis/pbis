<?php

namespace App\Http\Controllers;


use App\Payroll;
use Illuminate\Http\Request;

class PayrollController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Payroll::latest()->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response`
     */
    public function store(Request $request)
    {
        
        
return Payroll::create([
    'name'=> $request['name'],   
'current_activity'=> $request['current_activity'],
'amount'=> $request['amount'],
'tax'=> $request['tax'],
'acknowledgment'=> $request['acknowledgment'],
//'user_id'=>$request['user_id']

       ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $payroll = Payroll::findOrFail($id);
        
        $payroll ->update($request->all());
        return['message'=>'Updated the payroll information'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $payroll = Payroll::findOrFail($id);
 
        $payroll -> delete();
        
        return['message'=>'Payroll deleted'];
    }
}

