<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Department;
use Illuminate\Support\Facades\Validator;


class DepartmentController extends Controller
{
    /**
     * Return a list of departments.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Department::all());
    }

    public function count()
    {
        return response(Department::all()->count());
    }

    /**
     * Store a newly created department in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:departments|max:100',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return response(['department' => 'error', 'error' => $validator->errors()]);
        }

        Department::create([
           'name' => $request->name,
            'description' => $request->description
        ]);

        return response(['department' => 'created']);
    }

    /**
     * Update the specified department in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return response(['department' => 'error', 'error' => $validator->errors()]);
        }

        Department::find($id)->update(['name' => $request->name, 'description' => $request->description]);

        return response(['department' => 'updated']);
    }

    /**
     * Remove the specified department from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $department = Department::with('users')->find($id);

        if ($department->users) {
            $department->delete();
        }

        return response(['department' => 'deleted', 'users' => $department]);
    }
}
