<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Work;
use Illuminate\Support\Facades\Validator;

class WorkController extends Controller
{
   /**
     * Return a list of work.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Work::all());
    }

    /**
     * Store a newly created work in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'image' => 'required',
            'pdf' => 'required',
            'description' => 'required',
            'department_id' => 'required',
        ]);

        // TODO:: 

        if ($validator->fails()) {
            return response(['work' => 'error', 'error' => $validator->errors()]);
        }

        Work::create([
            'title' => $request->title,
            'image' => $request->image,
            'pdf' => $request->pdf,
            'description' => $request->description,
            'department_id' => $request->department_id,
        ]);

        return response(['work' => 'created']);
    }

    /**
     * return the specified work data.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(work::find($id));
    }

    /**
     * Update the specified work in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified work from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
