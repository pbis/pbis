<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PrisonerMovement;
use Illuminate\Support\Facades\Validator;

class PrisonerMovementController extends Controller
{
    public function index()

    {
        return response(PrisonerMovement::all());
    }

    public function store (Request $request)

    {
        $validator = validator::make ($request->all(),[
            'prisoner_id' => 'required',
            'driver_id' => 'required',
            'guard1_id' => 'required',
            'guard2_id' => 'required',
            'guard3_id' => 'required',
            'from' => 'required',
            'to' => 'required'

        ]);

        if ($validator -> fails()){
            return response(['PrisonerMovement' => 'error', 'error' =>
                $validator->errors()]);
        }

        PrisonerMovement::create([
            'prisoner_id' => $request->prisoner_id,
            'driver_id' => $request->driver_id,
            'guard1_id' => $request->guard1_id,
            'guard2_id' => $request->guard2_id,
            'guard3_id' => $request->guard3_id,
            'from' => $request->from,
            'to' => $request->to

        ]);

        return response(['PrisonerMovement' => 'created'], 200);

    }

    public function update(Request $request, $id)
    {
    $validator = Validator::make($request->all(),[
        'prisoner_id' =>'required',
        'driver_id' =>'required',
        'guard1_id' =>'required',
        'guard2_id' =>'required',
        'guard3_id' =>'required',
        'from' =>'required',
        'to' =>'required'
    ]);

    if ($validator->fails()){
        return response(['PrisonerMovement' =>'error', 'error' =>$validator->errors()]);
    }

    PrisonerMovement::find($id)->update(['prisoner_id' => $request-> prisoner_id,
    'driver_id' => $request-> driver_id,
    'guard1_id' => $request-> guard1_id,
    'guard2_id' => $request-> guard2_id,
    'guard3_id' => $request-> guard3_id,
    'from' => $request-> from,
    'to' => $request-> to
    ]);
        return response(['PrisonerMovement' => 'updated']);
    }

    public function destroy($id){
    PrisonerMovement::find($id)->delete();
    return response (['prisoner movement' => 'deleted']);
    }
}

