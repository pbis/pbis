<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\MedicalStatus;
use Illuminate\Support\Facades\Validator;
class MedicalStatusController extends Controller
{
     /**
     * Return a list of medical status.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(MedicalStatus::all());
    }

    /**
     * Store a newly created medical status in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'surname' => 'required',
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'sex' => 'required',
            'doctor' => 'required',
            'nurse' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return response(['medicalStatus' => 'error', 'error' => $validator->errors()]);
        }

        MedicalStatus::create([
            'id' => $request->id,
            'surname' => $request->surname,
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'sex' => $request->sex,
            'doctor' => $request->doctor,
            'nurse' => $request->nurse,
            'description' => $request->description,
        ]);

        return response(['medicalStatus' => 'created']);
    }

    /**
     * return the specified medicalStatus data.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(medicalStatus::find($id));
    }

    /**
     * Update the specified medicalStatus in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified medicalStatus from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
