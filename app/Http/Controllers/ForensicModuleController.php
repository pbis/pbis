<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ForensicModule;
use Illuminate\Support\Facades\Validator;

class ForensicModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(ForensicModule::with('crime')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'crime_id' => 'required',
            'file' => 'required'
        ]);

        if ($validator->fails()) {
            return response(['forensicModule' => 'error', 'error' => $validator->errors()]);
        }

        $file_name = time().'.'.$request->file->getClientOriginalExtension();
        $request->file->move(public_path('files/forensic'), $file_name);

        ForensicModule::create([
           'title' => $request->title,
            'description' => $request->description,
            'crime_id' => $request->crime_id,
            'file' => $file_name
        ]);

        return response(['forensicModule' => 'created']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(ForensicModule::with('crime')->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $forensic_module = ForensicModule::find($id);
        // dd($forensic_module->count());

        if ($forensic_module->count()) {
            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'description' => 'required',
                'crime_id' => 'required'
            ]);
    
            if ($validator->fails()) {
                return response(['forensicModule' => 'error', 'error' => $validator->errors()]);
            }

            $forensic_module->update([
                'title' => $request->title,
                'description' => $request->description,
                'crime_id' => $request->crime_id
            ]);

            return response()->json(['forensicModule' => "updated"]);
        }
        return response(['forensicModule' => "can't find the given forensic module"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ForensicModule::find($id)->delete();
        return response()->json(["forensicModule" => "deleted"]);
    }
}
