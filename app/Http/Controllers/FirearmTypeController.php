<?php

namespace App\Http\Controllers;

use App\FirearmType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FirearmTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(FirearmType::with('firearms')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return response(['firearm' => 'error', 'error' => $validator->errors()]);
        }
        FirearmType::create([
            'name' => $request->name
        ]);

        return response()->json(true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FirearmType  $firearmType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return response(['firearm' => 'error', 'error' => $validator->errors()]);
        }

        FirearmType::find($id)->update(["name" => $request->name]);
        return response()->json(true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FirearmType  $firearmType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        FirearmType::find($id)->delete();
        return response()->json(true);
    }
}
