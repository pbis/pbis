<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\training;
use Illuminate\Support\Facades\Validator;

class trainingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(training::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = validator::make ($request-> all(), [
            'title'=> 'required',
            'description'=> 'required',
            'date'=> 'required',
            'user_id'=> 'required',
            'department'=> 'required'
            ]);

        if ($validator -> fails()){
            return response(['trainingController' =>'error','error' =>$validator->errors()]);
    }

        training::create([
            'title' => $request-> title,
            'description'=> $request-> description,
            'date'=> $request-> date,
            'user_id'=> $request-> user_id,
            'department'=> $request-> department
        ]);

        return response(['trainingController' => 'created']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'title'=> 'required',
            'description'=> 'required',
            'date'=> 'required',
            'user_id'=> 'required',
            'department'=> 'required'
        ]);

        if ($validator->fails()){
            return response(['trainingController' => 'error', 'error' => $validator-> errors()]);
        }

        training::find($id)->update([
            'title' => $request->title,
            'description'=> $request->description,
            'date'=> $request->date,
            'user_id'=> $request->user_id,
            'department'=> $request->department
        ]);

        return response(['trainingController' => 'updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    training::find($id)->delete();
    return response(['training'=>'deleted']);
    }
}
