<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Firearm;
use Illuminate\Support\Facades\Validator;

class FirearmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Firearm::with(['registrar', 'type'])->get());
    }

    public function count()
    {
        return response(Firearm::all()->count());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'owner_full_name' => 'required',
            'owner_address' => 'required',
            'code' => 'required',
            'type_id' => 'required',
            'registered_by' => 'required'
        ]);

        if ($validator->fails()) {
            return response(['firearm' => 'error', 'error' => $validator->errors()]);
        }

        // TODO:: store the biometric data here
        //      - store the biometric data id in $biometric_data_id

        $biometric_data_id = 1;

        Firearm::create([
           'owner_full_name' => $request->owner_full_name,
            'owner_address' => $request->owner_address,
            'code' => $request->code,
            'type_id' => $request->type_id,
            'biometric_capture_id' => $biometric_data_id,
            'registered_by' => $request->registered_by,
            'verified' => 0
        ]);

        return response(['firearm' => 'created']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(Firearm::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $firearm = Firearm::find($id);
        if ($firearm) {
            $validator = Validator::make($request->all(), [
                'owner_full_name' => 'required',
                'owner_address' => 'required',
                'code' => 'required',
                'type_id' => 'required',
                'registered_by' => 'required'
            ]);
    
            if ($validator->fails()) {
                return response(['firearm' => 'error', 'error' => $validator->errors()]);
            }

            $firearm->update([
                'owner_full_name' => $request->owner_full_name,
                 'owner_address' => $request->owner_address,
                 'code' => $request->code,
                 'type_id' => $request->type_id,
                 'registered_by' => $request->registered_by
             ]);

            return response(['firearm' => 'updated']);
        }
        return response(["firearm" => "can't find the specified firearm"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Firearm::find($id)->delete();

        return response(['firearm' => 'deleted']);
    }

    public function verify($id)
    {
        Firearm::find($id)->update([
            'verified' => 1
        ]);
        return response()->json(true);
    }

    public function unverify($id)
    {
        Firearm::find($id)->update([
            'verified' => 0
        ]);

        return response()->json(true);
    }
}
