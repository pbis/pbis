<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\crime;
use Illuminate\Support\Facades\Validator;
class CrimeController extends Controller
{
   /**
     * Return a list of crime.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(crime::all());
    }

    /**
     * Store a newly created crime in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'crime_nature_id' => 'required',
            'crime_street_address' => 'required',
            'crime_city' => 'required',
            'crime_state' => 'required',
            'crime_zip' => 'required',
            'investigation_officer' => 'required',
            'date_of_crime' => 'required',
            'victim_id' => 'required',
            'victim_surname' => 'required',
            'victim_fname' => 'required',
            'victim_mname' => 'required',
            'victim_lname' => 'required',
            'victim_street_Address' => 'required',
            'victim_city' => 'required',
            'victim_state' => 'required',
            'victim_zip' => 'required',
            'victim_nationality' => 'required',
            'victim_date_of_birth' => 'required',
            'victim_age' => 'required',
            'victim_sex' => 'required',
            'case_number' => 'required',
            'witness_fname' => 'required',
            'witness_lname' => 'required',
            'witness_mname' => 'required',
            'witness_street_address' => 'required',
            'witness_city' => 'required',
            'witness_state' => 'required',
            'witness_zip' => 'required',
            'witness_nationality' => 'required',
            'witness_date_of_birth' => 'required',
            'witness_age' => 'required',
            'criminal_id' => 'required',
            'criminal_crime_description' => 'required',
            'criminal_fname' => 'required',
            'criminal_mname' => 'required',
            'criminal_lname' => 'required',
            'criminal_street_address' => 'required',
            'criminal_city' => 'required',
            'criminal_state' => 'required',
            'criminal_zip' => 'required',
            'criminal_nationality' => 'required',
            'criminal_date_of_birth' => 'required',
            'criminal_Date_of_arrest' => 'required',
            'criminal_age' => 'required',
            'nature_of_crime' => 'required',
            
    
        ]);

        if ($validator->fails()) {
            return response(['crime' => 'error', 'error' => $validator->errors()]);
        }

        Crime::create([
            'crime_nature_id' => $request->crime_nature_id,
            'crime_street_address' => $request->crime_street_address,
            'crime_city' => $request->crime_city,
            'crime_state' => $request->crime_state,
            'crime_zip' => $request->crime_zip,
            'investigation_officer' => $request->investigation_officer,
            'date_of_crime' => $request->date_of_crime,
            'victim_id' => $request->phone_victim_id,
            'victim_surname' => $request->victim_surname,
            'victim_fname' => $request->victim_fname,
            'victim_mname' => $request->victim_mname,
            'victim_lname' => $request->victim_lname,
            'victim_street_Address' => $request->victim_street_Address,
            'victim_city' => $request->victim_city,
            'victim_state' => $request->victim_state,
            'victim_zip' => $request->victim_zip,
            'victim_nationality' => $request->victim_nationality,
            'victim_date_of_birth' => $request->victim_date_of_birth,
            'victim_age' => $request->victim_age,
            'victim_sex' => $request->victim_sex,
            'case_number' => $request->case_number,
            'witness_fname' => $request->witness_fname,
            'witness_lname' => $request->witness_lname,
            'witness_mname' => $request->witness_mname,
            'witness_street_address' => $request->witness_street_address,
            'witness_city' => $request->witness_city,
            'witness_state' => $request->witness_state,
            'witness_zip' => $request->witness_zip,
            'witness_nationality' => $request->witness_nationality,
            'witness_date_of_birth' => $request->witness_date_of_birth,
            'witness_age' => $request->witness_age,
            'criminal_id' => $request->criminal_id,
            'criminal_crime_description' => $request->criminal_crime_description,
            'criminal_fname' => $request->criminal_fname,
            'criminal_mname' => $request->criminal_mname,
            'criminal_lname' => $request->email,
            'criminal_street_address' => $request->criminal_street_address,
            'criminal_city' => $request->criminal_city,
            'criminal_state' => $request->criminal_state,
            'criminal_zip' => $request->criminal_zip,
            'criminal_nationality' => $request->criminal_nationality,
            'criminal_date_of_birth' => $request->criminal_date_of_birth,
            'criminal_Date_of_arrest' => $request->criminal_Date_of_arrest,
            'criminal_age' => $request->criminal_age,
            'nature_of_crime' => $request->nature_of_crime,
            'criminal_id' => $request->criminal_id,

        ]);

        return response(['crime' => 'created']);
    }

    /**
     * return the specified crime data.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(crime::find($id));
    }

    /**
     * Update the specified crime in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified crime from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
