<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permission;
use Illuminate\Support\Facades\Validator;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Permission::all());
    }

    /**
     * Store a newly created permission in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:departments|max:100'
        ]);

        if ($validator->fails()) {
            return response(['permission' => 'error', 'error' => $validator->errors()]);
        }

        Permission::create([
            'name' => $request->name
        ]);

        return response(['permission' => 'created']);
    }

    /**
     * Update the specified permission in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:departments|max:100'
        ]);

        if ($validator->fails()) {
            return response(['permission' => 'error', 'error' => $validator->errors()]);
        }

        Permission::find($id)->update(['name' => $request->name]);

        return response(['permission' => 'updated']);
    }

    /**
     * Remove the specified permission from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Permission::find($id)->delete();

        return response(['department' => 'deleted']);
    }
}
