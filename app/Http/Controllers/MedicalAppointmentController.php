<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\MedicalAppointment;
use Illuminate\Support\Facades\Validator;
class MedicalAppointmentController extends Controller
{
    /**
     * Return a list of medical appointment.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(MedicalAppointment::all());
    }

    /**
     * Store a newly created medical appointment in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'surname' => 'required',
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'sex' => 'required',
            'doctor' => 'required',
            'nurse' => 'required',
            'duration' => 'required',
        ]);

        if ($validator->fails()) {
            return response(['medicalAppointment' => 'error', 'error' => $validator->errors()]);
        }

        MedicalAppointment::create([
            'id' => $request->id,
            'surname' => $request->surname,
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'sex' => $request->sex,
            'doctor' => $request->doctor,
            'nurse' => $request->nurse,
            'duration' => $request->duration,
        ]);

        return response(['medicalAppointment' => 'created']);
    }

    /**
     * return the specified medicalAppointment data.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(medicalAppointment::find($id));
    }

    /**
     * Update the specified medicalAppointment in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified medicalAppointment from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
