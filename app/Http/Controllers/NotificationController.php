<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Notification;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Department;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    /**
     * Display a listing of the notification for the user or department.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $user = User::select(["id", "department_id"])->with("notifications")->find($id);
        $department = Department::select("id")->with("notifications")->find($user->department_id);
        // dd($user);
        // $notifications = Notification::with([
        //     'departments' => function($q) use ($user) {
        //     $q->select('notification_id')->where('department_id', $user->department_id);
        // }, 'users' => function($q) use ($user) {
        //     $q->select('notification_id')->where('user_id', $user->id);
        // }])->get();

        // $notifications = array_merge((array) $department->notifications, (array) $user->notifications);

        return response()->json(["user" => $user->notifications, "department" => $department->notifications]);
    }

    /**
     * Store a newly created notification in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'header' => 'required',
            'description' => 'required',
            'link' => 'required'
        ]);

        if ($validator->fails()) {
            return response(['notification' => 'error', 'error' => $validator->errors()]);
        }

        $notification = Notification::create([
            'header' => $request->header,
            'description' => $request->description,
            'link' => $request->link
        ]);

        $notification->users()->attach($request->users);

        $notification->departments()->attach($request->departments);

        return response(['notification' => 'created']);
    }

    /**
     * Update the specified notification in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'header' => 'required',
            'description' => 'required',
            'link' => 'required'
        ]);

        if ($validator->fails()) {
            return response(['notification' => 'error', 'error' => $validator->errors()]);
        }

        $notification = Notification::find($id);

        $notification->users()->sync($request->users);

        $notification->departments()->sync($request->departments);

        $notification->update([
            'header' => $request->header,
            'description' => $request->description,
            'link' => $request->link
        ]);

        return response(['notification' => 'updated']);    }

    /**
     * Remove the specified notification from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Notification::find($id)->delete();

        return response(['notification' => 'deleted']);    }
}
