<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Role;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /**
     * Return list of roles.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Role::all());
    }

    /**
     * Return a list of roles with permissions.
     *
     * @return \Illuminate\Http\Response
     */
    public function roleWithPermission()
    {
        return response(Role::with('permissions')->get());
    }

    /**
     * Store a new Role in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:departments|max:100',
            'permissions' => 'required'
        ]);

        if ($validator->fails()) {
            return response(['role' => 'error', 'error' => $validator->errors()]);
        }

        Role::create([
            'name' => $request->name
        ])->permissions()->attach($request->permissions);

        return response(['role' => 'created']);
    }

    /**
     * Update the specified role in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:departments|max:100',
            'permissions' => 'required'
        ]);

        if ($validator->fails()) {
            return response(['role' => 'error', 'error' => $validator->errors()]);
        }

        $role = Role::find($id);

        $role->permissions()->sync($request->permissions);

        $role->update(['name' => $request->name]);

        return response(['role' => 'updated']);
    }

    /**
     * Remove the specified role from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Role::find($id)->delete();

        return response(['role' => 'deleted']);
    }
}
