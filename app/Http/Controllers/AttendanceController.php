<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Attendance;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class AttendanceController extends Controller
{
    /**
     * return user attendance for a given month.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @param  string  $year
     * @param  string  $month
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id, $year, $month) {
        $attendance = Attendance::where('user_id', $id)->whereYear('created_at', '=', $year)->whereMonth('created_at', '=', $month)->get();
        return response($attendance);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'status' => 'required'
        ]);

        if ($validator->fails()) {
            return response(['attendance' => 'error', 'error' => $validator->errors()]);
        }

        $attendance = Attendance::where('user_id', $request->user_id)->whereDate('created_at', '=', Carbon::today()->toDateString())->get();

        if ($attendance->count() > 0) {
            return response(['attendance' => 'error', 'error' => 'already submitted', 'attendance_data' => $attendance]);
        }

        Attendance::create([
           'user_id' => $request->user_id,
            'status' => $request->status
        ]);

        return response(['attendance' => 'created']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'status' => 'required'
        ]);

        if ($validator->fails()) {
            return response(['attendance' => 'error', 'error' => $validator->errors()]);
        }

        Attendance::find($id)->update([
            'user_id' => $request->user_id,
             'status' => $request->status
         ]);

        return response(['attendance' => 'updated']);
    }
}
