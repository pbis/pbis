<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\HealthcareProfessional;
use Illuminate\Support\Facades\Validator;
class HealthcareProfessionalController extends Controller
{
    /**
     * Return a list of healthcare professional.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(HealthcareProfessional::all());
    }

    /**
     * Store a newly created healthcare professional in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'surname' => 'required',
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'sex' => 'required',
            'age' => 'required',
            'photo' => 'required',
            'phone_number' => 'required',
            'education' => 'required',
            'current_activity' => 'required',
            'email' => 'required',
        ]);

        if ($validator->fails()) {
            return response(['healthcareProfessional' => 'error', 'error' => $validator->errors()]);
        }

        HealthcareProfessional::create([
            'id' => $request->id,
            'surname' => $request->surname,
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'sex' => $request->sex,
            'age' => $request->age,
            'photo' => $request->photo,
            'education' => $request->phone_education,
            'current_activity' => $request->current_activity,
            'email' => $request->email,
        ]);

        return response(['healthcareProfessional' => 'created']);
    }

    /**
     * return the specified healthcare Professional data.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(healthcareProfessional::find($id));
    }

    /**
     * Update the specified healthcare Professional in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified healthcare professional from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
