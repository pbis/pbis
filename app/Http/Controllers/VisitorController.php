<?php

namespace App\Http\Controllers;

use App\Visitor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VisitorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Visitor::with('prisoner')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'full_name' => 'required',
            'phone' => 'required',
            'prisoner_id' => 'required',
            'relation' => 'required'
        ]);

        if ($validator->fails()) {
            return response(['visitor' => 'error', 'error' => $validator->errors()]);
        }

        Visitor::create([
           'full_name' => $request->full_name,
            'phone' => $request->phone,
            'prisoner_id' => $request->prisoner_id,
            'relation' => $request->relation
        ]);

        return response(['visitor' => 'created']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(Visitor::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'full_name' => 'required',
            'phone' => 'required',
            'prisoner_id' => 'required',
            'relation' => 'required'
        ]);

        if ($validator->fails()) {
            return response(['visitor' => 'error', 'error' => $validator->errors()]);
        }

        Visitor::find($id)->update(['full_name' => $request->full_name, 'phone' => $request->phone, 'prisoner_id' => $request->prisoner_id, 'relation' => $request->relation]);

        return response(['visitor' => 'updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Visitor::find($id)->delete();

        return response(['visitor' => 'deleted']);
    }
}
