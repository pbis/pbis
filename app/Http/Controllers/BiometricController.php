<?php

namespace App\Http\Controllers;

use App\Biometric;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BiometricController extends Controller
{
    
    public function index()
    {
        return response()->json(Biometric::all());
    }
    public function getBiometrics($id)
    {
        return response()->json(Biometric::where('prisoner_id', $id)->get());
    }

    public function store(Request $request)
    {
     $validator = Validator::make($request->all(), [
            'prisoner_id' => 'required',
            'photo' => 'required',
            'name' => 'required',
            'fingerprint' => 'required'
        ]);     

        if ($validator->fails()) {
            return response(['biometric' => 'error', 'error' => $validator->errors()]);
        }

        $photo_name = time().'.'.$request->photo->getClientOriginalExtension();
        $request->photo->move(public_path('images/biometric'), $photo_name);

        Biometric::create([
           'prisoner_id' => $request->prisoner_id,
            'photo' => $photo_name,
            'name' => $request->name,
            'fingerprint' => $request->fingerprint
        ]);

        return response(['biometric' => 'created']);

    }
    
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return response()->json(Biometric::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $validator = Validator::make($request->all(), [
            'prisoner_id' => 'required',
            'photo' => 'required',
            'name' => 'required',
            'fingerprint' => 'required'
        ]);

        if ($validator->fails()) {
            return response(['biometric' => 'error', 'error' => $validator->errors()]);
        }

        // TODO :: upload the photo and store the path in $photo_name

        $photo_name = 'photo';

        Biometric::find($id)->update([
                'prisoner_id' => $request->prisoner_id,
                'photo' => $request->photo,
                'name' => $request->name,
                'fingerprint' => $request->fingerprint
            ]);

        return response(['biometric' => 'updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
      Biometric::find($id)->delete();
        return response()->json(["biometric" => "deleted"]);
    }
}

