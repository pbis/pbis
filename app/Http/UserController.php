<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Return a list of users.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(User::with(['role', 'department'])->get());
    }

    public function getByDepartment($department_id)
    {
        return response()->json(User::where('department_id', $department_id)->get());
    }

    public function count()
    {
        return response(User::all()->count());
    }

    /**
     * Store a newly created user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'surname' => 'required',
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'sex' => 'required',
            'nationality' => 'required',
            'birth_place' => 'required',
            'birth_date' => 'required',
            'department_id' => 'required',
            'role_id' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8'
        ]);

        if ($validator->fails()) {
            return response(['user' => 'error', 'error' => $validator->errors()]);
        }

        User::create([
            'surname' => $request->surname,
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'sex' => $request->sex,
            'nationality' => $request->nationality,
            'birth_place' => $request->birth_place,
            'birth_date' => $request->birth_date,
            'department_id' => $request->department_id,
            'role_id' => $request->role_id,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'active' => 1
        ]);

        return response(['user' => 'created'], 200);
    }

    /**
     * return the specified user data.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(User::whereId($id)->first());
    }

    /**
     * Update the specified user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if($user->count()) {
            $validator = Validator::make($request->all(), [
                'surname' => 'required',
                'first_name' => 'required',
                'middle_name' => 'required',
                'last_name' => 'required',
                'sex' => 'required',
                'nationality' => 'required',
                'birth_place' => 'required',
                'birth_date' => 'required',
                'department_id' => 'required',
                'role_id' => 'required'
            ]);
    
            if ($validator->fails()) {
                return response(['user' => 'error', 'error' => $validator->errors()]);
            }

            $update = $user->update([
                'surname' => $request->surname,
                'first_name' => $request->first_name,
                'middle_name' => $request->middle_name,
                'last_name' => $request->last_name,
                'sex' => $request->sex,
                'nationality' => $request->nationality,
                'birth_place' => $request->birth_place,
                'birth_date' => $request->birth_date,
                'department_id' => $request->department_id,
                'role_id' => $request->role_id
            ]);

            // dd($request);

            if ($update) {
                return response(['user' => 'updated']);
            }
        }
    }

    /**
     * Change user password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request, $id)
    {
        $user = User::find($id)->get();

        if($user->count()) {
            $validator = Validator::make($request->all(), [
                'password' => 'required|min:8',
                'old_password' => 'required'
            ]);
    
            if ($validator->fails()) {
                return response(['user' => 'error', 'error' => $validator->errors()]);
            }

            if (Auth::user()->getAuthPassword() === $request->old_password) {
                $user->update([
                    'password' => bcrypt($request->password)
                ]);
            } else {
                return response(['user' => 'old password is not correct']);
            }
            return response(['user' => 'password changed']);
        } else {
            return response(['user' => "user doesn't exist"]);
        }
    }

    /**
     * Disable the specified user account.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deactivate($id)
    {
        User::find($id)->update(['active' => 0]);
        return response(['user' => 'disabled']);
    }

    /**
     * Activate the specified user account.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        User::find($id)->update(['active' => 1]);
        return response(['user' => 'activated']);
    }
}
