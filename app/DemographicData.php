<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DemographicData extends Model
{
    protected $guarded = [];
}
