<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $guarded = [];

    public function users() {
        return $this->belongsToMany('App\User')->withPivot('status');
    }

    public function departments() {
        return $this->belongsToMany('App\Department')->withPivot('status');
    }
}
