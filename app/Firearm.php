<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Firearm extends Model
{
    protected $guarded = [];

    public function registrar()
    {
        return $this->belongsTo("App\User", "registered_by");
    }

    public function type()
    {
        return $this->belongsTo("App\FirearmType", "type_id");
    }
}
