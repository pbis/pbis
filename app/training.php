<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class training extends Model
{
    protected $guarded = [];


    public function users() {
        return $this ->hasMany('App\User','training_id');
    }
    public function department (){
        return $this->belongsToMany('App\Department');
    }
    public function notifications(){
        return $this->belongsToMany('App\Notification');
    }

    protected $table= 'trainings';
}
