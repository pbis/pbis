<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FirearmType extends Model
{
    protected $guarded = [];

    public function firearms()
    {
        return $this->hasMany("App\Firearm", "type_id");
    }
}
