<?php

namespace App;
use Illuminate\Database\Eloquent\Model;



class Payroll extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'current_activity', 'amount','tax','acknowledgment'
    ];

   // public function user()
    //{
       // return $this->belongsTo('App/User');
   // }

   
}
