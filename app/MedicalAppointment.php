<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicalAppointment extends Model
{
    protected $guarded = [];
    protected $table = 'medicalAppointments';
}
