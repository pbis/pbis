<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForensicModule extends Model
{
    protected $guarded = [];

    public function crime()
    {
        return $this->belongsTo('App\crime');
    }
}
