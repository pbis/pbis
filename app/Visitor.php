<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    protected $guarded = [];
    
    public function prisoner()
    {
        return $this->belongsTo('App\Prisoner');
    }
}
