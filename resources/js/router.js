import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/Login'
import Dashboard from './views/Dashboard'
import User from './views/user/User'
import Users from './views/user/Users'
import AddUser from './views/user/AddUser'
import EditUser from './views/user/EditUser'
import Roles from './views/Roles'
import Prisoners from './views/Prisoners'
import Permissions from './views/Permissions'
import Departments from './views/Departments'
import Profile from './views/user/Profile'
import PrisonerMovements from './views/PrisonerMovement'
<<<<<<< HEAD
import Visitors from './views/visitor/Visitors'
import AddVisitor from './views/visitor/Add'
import ListVisitors from './views/visitor/List'
import Forensic from './views/forensic/Forensics'
import AddForensic from './views/forensic/Add'
import ListForensic from './views/forensic/List'
import ShowForensic from './views/forensic/Show'
import Firearm from './views/firearm/firearm'
import AddFirearm from './views/firearm/add'
import EditFirearm from './views/firearm/edit'
import ListFirearm from './views/firearm/list'
import Demographics from './views/demographic/Demographics'
import AddDemographics from './views/demographic/AddDemographics'
import Attendance from './views/attendance/Attendance'
import Biometric from './views/Biometric'
import Biometrics from './views/biometric/Biometrics'
import AddBiometrics from './views/biometric/AddBiometrics'
import Payroll from './views/Payroll'
=======
import MedicalAppointment from './views/MedicalAppointment'
import HelthcareProfessional from './views/HealthcareProfessional'
>>>>>>> origin/abeni

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            redirect: '/login'
        }, {
            path: '/login/:path?',
            name: 'login',
            component: Login
        }, {
            path: '/dashboard',
            name: 'dashboard',
            component: Dashboard,
            meta: {
                requireAuth: true
            }
        }, {
            path: '/roles',
            name: 'roles',
            component: Roles,
            meta: {
                requireAuth: true
            }
        }, {
            path: '/permissions',
            name: 'permissions',
            component: Permissions,
            meta: {
                requireAuth: true
            }
        }, {
            path: '/departments',
            name: 'departments',
            component: Departments,
            meta: {
                requireAuth: true
            }
        }, {
            path: '/prisoners',
            name: 'prisoners',
            component: Prisoners,
            meta: {
                requireAuth: true
            }
        }, 
        
        {
            path: '/biometrics',
            name: 'biometrics',
            component: Biometrics,
            meta: {
                requireAuth: true
            }
        },
        {
            path: '/AddBiometrics/:userId',
            name: 'addBiometrics',
            component: AddBiometrics,
            props: true,
            meta: {
                requireAuth: true
            }
        },
        {
            path: '/payroll',
            name: 'payroll',
            component: Payroll,
            meta: {
                requireAuth: true
            }
        },
        
        {
            path: '/prisonerMovements',
            name: 'prisonerMovements',
            component: PrisonerMovements,
            meta: {
                requireAuth: true
            }
        }, {
<<<<<<< HEAD
            path: '/visitors',
            component: Visitors,
            children: [
                {
                    path: '',
                    component: ListVisitors,
                    name: 'listVisitors',
                    meta: {
                        requireAuth: true
                    }
                }, {
                    path: 'add',
                    component: AddVisitor,
                    name: 'addVisitor',
                    meta: {
                        requireAuth: true
                    }
                }
            ]
        }, {
            path: '/demographics',
            name: 'demographics',
            component: Demographics,
            props: true,
            meta: {
                requireAuth: true
            }
        }, {
            path: '/attendance',
            name: 'attendance',
            component: Attendance,
            props: true,
=======
            path: '/medicalAppointment',
            name: 'medicalAppointment',
            component: MedicalAppointment,
>>>>>>> origin/abeni
            meta: {
                requireAuth: true
            }
        }, {
<<<<<<< HEAD
            path: '/addDemographics/:userId',
            name: 'addDemographics',
            component: AddDemographics,
            props: true,
            meta: {
                requireAuth: true
            }
        }, {
            path: '/forensics',
            component: Forensic,
            children: [
                {
                    path: '',
                    component: ListForensic,
                    name: 'listForensic',
                    meta: {
                        requireAuth: true
                    }
                }, {
                    path: 'show/:id',
                    component: ShowForensic,
                    name: 'showForensic',
                    props: true,
                    meta: {
                        requireAuth: true
                    }
                }, {
                    path: 'add',
                    component: AddForensic,
                    name: 'addForensic',
                    meta: {
                        requireAuth: true
                    }
                }]
        }, {
            path: '/firearms',
            component: Firearm,
            children: [
                {
                    path: '',
                    name: 'firearms',
                    component: ListFirearm,
                    meta: {
                        requireAuth: true
                    }

                }, {
                    path: 'add',
                    name: 'addFirearm',
                    component: AddFirearm,
                    meta: {
                        requireAuth: true
                    }
                },
                {
                    path: 'edit/:id',
                    name: 'editFirearm',
                    props: true,
                    component: EditFirearm,
                    meta: {
                        requireAuth: true
                    }
                }
            ],
=======
            path: '/healthcareProfessional',
            name: 'healthcareProfessional',
            component: HelthcareProfessional,
>>>>>>> origin/abeni
            meta: {
                requireAuth: true
            }
        }, {
            path: '/users',
            component: User,
            children: [
                {
                    path: '',
                    name: 'users',
                    component: Users,
                    meta: {
                        requireAuth: true
                    }
                }, {
                    path: 'add',
                    component: AddUser,
                    name: 'addUser',
                    meta: {
                        requireAuth: true
                    }
                }, {
                    path: 'edit/:id',
                    component: EditUser,
                    props: true,
                    name: 'editUser',
                    meta: {
                        requireAuth: true
                    }
                }, {
                    path: 'profile/:id',
                    component: Profile,
                    props: true,
                    name: 'profile',
                    meta: {
                        requireAuth: true
                    }
                }],

            meta: {
                requireAuth: true
            }
        }
    ],
    mode: 'history'
})