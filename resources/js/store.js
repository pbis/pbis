import Vue from 'vue'
import Vuex from 'vuex'
import router from './router'
import Axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        userData: {
            access_token: '',
            expires_in: null,
            user: null
        },
        loading: false,
        authenticating: false,
        notification: {
            type: '',
            value: '',
            visible: false
        },
        loadingMessage: '',
        permissions: [],
        role: '',
        authError: '',
        notifications: [],
        signupError: []
    },
    getters: {
        isAuthenticated(status) {
            return (status.userData.access_token !== '')
        }
    },
    mutations: {
        startLoading(state, payload) {
            state.loadingMessage = payload
            state.loading = true
        },
        stopLoading(state) {
            state.loadingMessage = ''
            state.loading = false
        },
        setUserData(state, paylode) {
            state.userData = paylode;
            localStorage.setItem('access_token', paylode.access_token)
            state.authError = '';
            state.authenticating = false;
            var permissions = [];

            for (let i = 0; i < paylode.user.role.permissions.length; i++) {
                permissions.push(paylode.user.role.permissions[i].name);
            }

            state.userData.permissions = permissions;

            if (paylode.path) {
                router.push({ 'name': paylode.path })
            } else {
                router.push('/dashboard')
            }
        },
        logout(state) {
            state.authenticating = false
            localStorage.removeItem('access_token')
            state.userData = { access_token: '', user: null }
            router.push('/login')
        },
        setNotifications(state, payload) {
            state.notifications = payload
        },
        showNotification(state, payload) {
            state.notification.type = payload.type;
            state.notification.value = payload.value;
            state.notification.visible = true;
        },
        hideNotification(state) {
            state.notification.visible = false;
        }
    },
    actions: {
        login({ commit, dispatch, state }, payload) {
            state.authenticating = true
            Axios.post('/api/auth/login', { email: payload.email, password: payload.password })
                .then(response => {
                    if (response.data.user) {
                        if (response.data.user.active == 0) {
                            state.authenticating = false;
                            state.authError = "Not authorized";
                        } else {
                            response.data['path'] = payload.path;
                            dispatch('getNotifications', response.data.user.id)
                            commit('setUserData', response.data);
                        }
                    } else {
                        state.authenticating = false;
                        state.authError = "Invalid e-mail or password";
                    }
                })
                .catch(error => {
                    state.authenticating = false;
                    state.authError = 'Invalid e-mail or password';
                    console.log(error)
                })
        },
        logout({ commit, state }) {
            Axios.post('/api/auth/logout?token=' + state.userData.access_token)
                .then(
                    res => {
                        commit('logout')
                    }
                ).catch(
                    error => {
                        console.log(error)
                    }
                )
        },
        getUser({ commit, dispatch }, payload) {
            Axios.post('/api/auth/me?token=' + payload.access_token)
                .then(
                    (res) => {
                        if (res.data) {
                            var data = {
                                access_token: payload.access_token,
                                user: res.data,
                                path: payload.path
                            }
                            dispatch('getNotifications', data.user.id)
                            commit('setUserData', data)

                            this.authError = ''

                            if (res.data.active == 0) {
                                commit('logout')
                            } else {
                                var data = {
                                    access_token: payload.access_token,
                                    user: res.data,
                                    path: payload.path
                                }
                                commit('setUserData', data)
                            }
                        } else {
                            commit('logout')
                        }
                    }
                )
                .catch(() => {
                    commit('logout')
                })
        },
        getNotifications({ commit }, payload) {
            Axios.get('/api/notifications/' + payload)
                .then(
                    res => {
                        var user = res.data.user;
                        var data = user.concat(res.data.department);

                        for(var i=0; i<data.length; ++i) {
                            for(var j=i+1; j<data.length; ++j) {
                                if(data[i].id === data[j].id)
                                    data.splice(j--, 1);
                            }
                        }

                        data.sort((a,b) => (a.created_at < b.created_at) ? 1 : ((b.created_at < a.created_at) ? -1 : 0));
                        commit('setNotifications', data)
                    }
                )
        },
        showNotification({ commit }, payload) {
            commit('showNotification', payload);
            setTimeout(() => {
                commit('hideNotification');
            }, 3000);
        }
    }
})
